import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import '../public/css/index.css'
import '../public/css/output.css'
import axios from 'axios';
import MD5 from 'blueimp-md5'
import GLOBAL_ from '@/components/Global'
import store from './store'
import './input.css'

Vue.config.productionTip = false

Vue.prototype.axios = axios;
Vue.prototype.MD5 = MD5;
Vue.prototype.$global = GLOBAL_;

new Vue({
    router,
    vuetify,
    store,
    render: h => h(App)
}).$mount('#app')
