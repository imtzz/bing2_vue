import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        show: false,
        image: Object
    },
    getters: {
        getShow(state) {
            return state.show;
        }
    },
    mutations: {
        setShow(state, show) {
            state.show = show;
        },
        originalShow(state) {
            console.log("true")
            state.show = true;
        },
        originalHidden(state) {
            console.log("false")
            state.show = false;
        },
        updateImageViewer(state, obj) {
            state.image = obj;
        }
    },
    actions: {},
    modules: {
    }
})

export default store;
